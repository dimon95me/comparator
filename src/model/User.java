package model;

/**
 * Created by Student on 17.04.2018.
 */
public class User implements Comparable<User>{
    private String name;
    private int age;
    private boolean isMale;

    public User(String name, int age, boolean isMale) {
        this.name = name;
        this.age = age;
        this.isMale = isMale;
    }


    @Override
    public int compareTo(User user) {
        if(isMale == user.isMale){
            return  age - user.age;
        } else {
            return isMale ? 1 : -1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (age != user.age) return false;
        if (isMale != user.isMale) return false;
        return name != null ? name.equals(user.name) : user.name == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + (isMale ? 1 : 0);
        return result;
    }

    public String toString(){
        return name+" "+age+" "+isMale+"\n";
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean getIsMale() {
        return isMale;
    }
}
