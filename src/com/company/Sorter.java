package com.company;

import model.User;

import java.util.Comparator;

/**
 * Created by Student on 17.04.2018.
 */
public class Sorter implements Comparator<User>{
    @Override
    public int compare(User o1, User o2) {
        return o1.getName().compareTo(o2.getName());
    }

}
