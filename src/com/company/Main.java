package com.company;

import model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
	// write your code here
        TreeSet<User> userList = new TreeSet<>();
        userList.add(new User("Vasya" , 19, true));
        userList.add(new User("Tanya" , 20,false));
        userList.add(new User("Kolya" , 27, true));
        userList.add(new User("Olya" , 19, false));
        userList.add(new User("Sanya" , 18, true));

       // Collections.sort(userList);
/*
        Sorter sorter = new Sorter();
        sorter.compare(userList.get(1),userList.get(2));
        System.out.println(userList);*/
        System.out.println(userList);


    }
}
